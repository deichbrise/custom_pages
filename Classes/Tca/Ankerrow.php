<?php

namespace Deichbrise\CustomPages\Tca;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Render a Template selection row
 *
 * @package Deichbrise\CustomPages\Tca
 */
class Ankerrow {

	/**
	 * Render a Content Variant row
	 *
	 * @param array $parameters
	 * @param mixed $parentObject
	 * @return string
	 */
	public function renderField(array &$parameters, &$parentObject) {

		// Vars

		$name  = $parameters['itemFormElName'];
		$value = $parameters['itemFormElValue'];


        if($name != '') {
            $ankers = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('anker, header, pid', 'tt_content', 'pid>=0');
            $selectBox = '<select name="' . htmlspecialchars($name) . '" class="select" onchange="' . htmlspecialchars(implode('', $parameters['fieldChangeFunc'])). '">' . LF;
            foreach($ankers as $key => $anker) {
                if($anker['anker'] != '') {
                    $title = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow('title', 'pages', 'uid=' . $anker['pid']);
                    $selected = '';
                    if($value==htmlspecialchars($anker['pid']) . '|' . htmlspecialchars($anker['anker'])) {
                        $selected = ' selected="selected" ';
                    }
                    if($anker['header'] == '') {
                        $header = '[No Title]';
                    } else {
                        $header = $anker['header'];
                    }
                    $selectBox .= '<option value="' . htmlspecialchars($anker['pid']) . '|' . htmlspecialchars($anker['anker']) . '" ' . $selected . '>' . $title['title'] . '[' . $anker['pid'] . ']' . ' / ' . $header .': ' . $anker['anker'] . '</option>' . LF;
                }
            }
            $selectBox .= '</select>' . LF;
        }

		return $selectBox;
	}
}