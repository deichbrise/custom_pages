﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt

.. _start:

=============================================================
Custom Pages
=============================================================

.. only:: html

	:Classification:
		custom_pages

	:Version:
		|release|

	:Language:
		en

	:Description:
		Add a new Custom Pages and make it accessible in the Page Viewhelper

	:Keywords:
		comma,separated,list,of,keywords

	:Copyright:
		2015

	:Author:
		Pascal Stammer

	:Email:
		stammer@deichbrise.de

	:License:
		This document is published under the Open Content License
		available from http://www.opencontent.org/opl.shtml

	:Rendered:
		|today|

	The content of this document is related to TYPO3,
	a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.

	**Table of Contents**

.. toctree::
	:maxdepth: 5
	:titlesonly:
	:glob:

	Introduction/Index
	User/Index
	Administrator/Index
	Configuration/Index
	Developer/Index
	KnownProblems/Index
	ToDoList/Index
	ChangeLog/Index
	Targets
