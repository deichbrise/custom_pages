# README #

### What is this Extension for? ###

* Add a new Page Type to the TYPO3 Pagetypes. You can define a anchor link in every Content Type. In the new Page Type you have access to this anchor-name and can choose by page, content-element-header and anchor-name. This Extension add also a new Viewhelper, which access the pages table and find there the anchor link and render a valid link in Frontend
* Version: Alpha 0.0.1

### How do I get set up? ###

* Install Extension and add Static Typoscript to your root template
* add your namespace to fluid templates {namespace c=Deichbrise\CustomPages\ViewHelpers} and replace all <f:link.page></f:link.page> viewhelper with <c:link.page></c:link.page>
* Add to all vhs menu view helper doktypes="1,2,3,4,5,6,7,8,9,10,116"