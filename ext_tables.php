<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}
t3lib_div::loadTCA('pages');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript/', 'Custom Pages');

$customPageIcon = \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath($_EXTKEY) . 'Resources/Public/Icons/pages_link.gif';
// Define a new doktype
$customPageDoktype = '116';
// Add the new doktype to the list of page types
$GLOBALS['PAGES_TYPES'][$customPageDoktype] = array(
    'type' => 'web',
    'icon' => $customPageIcon,
    'allowedTables' => '*'
);

$GLOBALS['TCA']['pages']['columns']['doktype']['config']['items'][] = array(
    'LLL:EXT:custom_pages/Resources/Private/Language/locallang.xlf:anker_page_type',
    $customPageDoktype,
    $customPageIcon
);

$GLOBALS['TCA']['pages_language_overlay']['columns']['doktype']['config']['items'][] = array(
    'LLL:EXT:custom_pages/Resources/Private/Language/locallang.xlf:anker_page_type',
    $customPageDoktype,
    $customPageIcon
);

\TYPO3\CMS\Backend\Sprite\SpriteManager::addTcaTypeIcon('pages', $customPageDoktype, $customPageIcon);
\TYPO3\CMS\Backend\Sprite\SpriteManager::addTcaTypeIcon('pages_language_overlay', $customPageDoktype, $customPageIcon);

// Add the new doktype to the list of types available from the new page menu at the top of the page tree
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addUserTSConfig(
    'options.pageTree.doktypesToShowInNewPageDragArea := addToList(' . $customPageDoktype . ')'
);

$anker['anker'] = array(
    'exclude' => 0,
    'label' => 'LLL:EXT:custom_pages/Resources/Private/Language/locallang_db.xlf:tx_custom_pages.anker',
    'config' => array(
        'type' => 'input',
        'size' => 50,
        'max' => 256,
    ),

);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tt_content', $anker);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tt_content', 'anker;;;;1-1-1', '', 'after:header');
$anker_pages['anker_link'] = array(
    'exclude' => 0,
    'label' => 'LLL:EXT:custom_pages/Resources/Private/Language/locallang_db.xlf:tx_custom_pages.anker',
    'config' => array(
        'type' => 'user',
        'userFunc' => 'Deichbrise\CustomPages\Tca\Ankerrow->renderField',
    ),

);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $anker_pages);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages_language_overlay', $anker_pages);

$GLOBALS['TCA']['pages']['types'][116]['showitem'] = substr_replace($GLOBALS['TCA']['pages']['types'][1]['showitem'], 'anker_link,', strpos($GLOBALS['TCA']['pages']['types'][1]['showitem'], 'title,') + strlen('title,'), 0);
$GLOBALS['TCA']['pages_languages_overlay']['types'][116]['showitem'] = substr_replace($GLOBALS['TCA']['pages']['types'][1]['showitem'], 'anker_link,', strpos($GLOBALS['TCA']['pages']['types'][1]['showitem'], 'title,') + strlen('title,'), 0);

