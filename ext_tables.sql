#
# Table structure for table 'pages'
#
CREATE TABLE pages (
  anker_link varchar(255) DEFAULT '' NOT NULL,
);

#
# Table structure for table 'tt_content'
#
CREATE TABLE tt_content (
  anker varchar(255) DEFAULT '' NOT NULL,
);

#
# Table structure for table 'pages_languages_overlay'
#
CREATE TABLE pages_language_overlay (
  anker_link varchar(255) DEFAULT '' NOT NULL,
);
